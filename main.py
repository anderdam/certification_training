import disclaimer
from aws_content.aws_practitioner_v2 import (
    read_json,
    shuffled_questions,
    score,
    start,
    add_question,
    remove_question,
    update_question,
)

from logo import logo
from disclaimer import disclaimer
import menus
from db.crud import (
    create_question,
    create_questions_from_json,
    get_question_by_tablename_and_id,
    get_question_by_tablename_and_text,
    get_all_questions,
    update_question,
    delete_question_by_id,
)
from db.generate_tables import create_table, drop


if __name__ == "__main__":
    json_file = "questions.json"
    # drop()
    create_table()
    create_questions_from_json(json_file=json_file, table_name="tb_aws_questions")

    # tbl = "tb_aws_questions"
    # print(get_all_questions(tbl))
    # print(len(get_all_questions(tbl)))
    # delete_question_by_id("tb_aws_questions", "AWS")

    # logo()
    # disclaimer()
    # chosen = menus.main_menu()
    # try:
    #     match chosen:
    #         case "1":
    #             aws_option = menus.aws_menu()
    #             match aws_option:
    #                 case "1":
    #                     aws_practitioner_option = menus.aws_practitioner_menu()
    #                     match aws_practitioner_option:
    #                         case "1":
    #                             start(shuffled_questions())
    #                         case "2":
    #                             print("Filter by question number.")
    #                         case "3":
    #                             print("Filter by question text.")
    #                         case "4":
    #                             add_question()
    #                         case "5":
    #                             remove_question()
    #                         case "6":
    #                             print("Quitting...")
    #                             exit()
    #                         case _:
    #                             print("Invalid option. Try again.")
    #                             print("")
    #                             chosen = menus.main_menu()
    #         case "2":
    #             menus.azure_menu()
    #         case "3":
    #             print("Quitting...")
    #             exit()
    #         case _:
    #             print("Invalid option. Try again.")
    #             print("")
    #             chosen = menus.main_menu()
    #
    # except KeyboardInterrupt:
    #     print("\nThanks for your time and good luck in your certification :) ")
