def disclaimer():
    print(
        """
        This script was made for study purposes only.

        Here you can find the questions and answers for the AWS Certified Cloud Practitioner exam, test your knowledge and improve your skills.

        Also, you can find informations about the new AWS Certified Data Engineer exam (DEA-C01), still in beta
        version to replace the old AWS Certified Big Data exam (DVA-C01), link: https://aws.amazon.com/certification/certified-data-engineer-associate/.

        This script is not affiliated with Amazon Web Services (AWS) in any way.
        """
    )
