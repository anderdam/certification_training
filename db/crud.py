import json
from pprint import pprint

import sqlalchemy as sa

from sqlalchemy.exc import NoResultFound
from db.generate_tables import AWSQuestions, AzureQuestions, engine
from sqlalchemy.orm import sessionmaker

Session = sessionmaker(bind=engine)
session = Session()


def get_model_class(table_name):
    if table_name == "tb_aws_questions":
        question_model = AWSQuestions
    elif table_name == "tb_az_questions":
        question_model = AzureQuestions
    else:
        raise ValueError(f"Invalid table name: {table_name}")
    return question_model


def read_json(json_file) -> dict:
    with open(json_file) as js:
        return json.load(js)


# Create
def create_question(table_name, certification, question_text, answer, link):
    try:
        existing_question = get_question_by_tablename_and_text(
            table_name=table_name, question_text=question_text
        )
        if existing_question is not None:
            print(
                f"Question {existing_question[0]} already exists."
            )  # Explicitly return a message
    except NoResultFound:
        new_question = get_model_class(table_name)(
            certification=certification.capitalize(),
            question=question_text.capitalize(),
            answer=answer.upper(),
            link=link,
        )
        session.add(new_question)
        session.commit()
        return f"Question {new_question.question_id} added."  # Return success message


def create_questions_from_json(json_file, table_name):
    questions = read_json(json_file)
    for question in questions.items():
        create_question(
            table_name, question[1][0], question[1][1], question[1][2], question[1][3]
        )


def get_question_by_tablename_and_id(table_name, question_id):
    with engine.begin() as conn:
        query = sa.text(f"SELECT * FROM {table_name} WHERE question_id = {question_id}")
        try:
            resultset = conn.execute(query)
            return resultset.mappings().all()
        except NoResultFound:
            return f"Question {question_id} not found."


def get_question_by_tablename_and_text(table_name, question_text):
    with engine.begin() as conn:
        query = sa.text(
            f"SELECT question_id FROM {table_name} WHERE question LIKE '%{question_text}%' "
        )
        try:
            resultset = conn.execute(query)
            return resultset.mappings().all()
        except NoResultFound:
            return f"Question {question_text} not found."


def get_all_questions(table_name):
    with engine.begin() as conn:
        query = sa.text(f"SELECT * FROM {table_name} ORDER BY question_id")
        try:
            resultset = conn.execute(query)
            return resultset.mappings().all()
        except NoResultFound:
            return f"There are no questions to show."


def update_question(table_name, question_id, **kwargs):
    question = (
        session.query(get_model_class(table_name=table_name))
        .filter_by(question_id=question_id)
        .one_or_none()
    )
    if question:
        for column, value in kwargs.items():
            setattr(question, column, value)
        session.commit()
        pprint(
            f"{question.question_id} updated from:\n"
            f' {question.question} to {kwargs["question"]}\n'
            f' {question.answer} to {kwargs["answer"]}\n'
            f' {question.link} to {kwargs["link"]}\n'
        )
        return f"Update question {question_id} successfully."
    else:
        return f"Question {question_id} not found. "


def delete_question_by_id(table_name, question_id):
    try:
        question = (
            session.query(get_model_class(table_name=table_name))
            .filter_by(question_id=question_id)
            .one()
        )
        session.delete(question)
        session.commit()
        pprint(f"Question {question.question_id} deleted.")
    except NoResultFound:
        pprint(f"Question {question_id} not found.")


if __name__ == "__main__":
    # create_question("teste3", "f", "http://teste")
    # create_questions_from_json()
    # print(get_question_by_id(127))
    # print(get_question_by_text("teste"))
    print(get_all_questions("tb_aws_questions"))
    # print(update_question(127, question="teste", answer="g", link="http://testexxx"))
    # print(get_question_by_id(127))
    # delete_question_by_id(126)
