from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import declarative_base

engine = create_engine("sqlite:///questions.db")  # For SQLite database

Base = declarative_base()


class Questions(Base):
    __abstract__ = True
    __tablename__ = "tb_questions"

    question_id = Column(Integer, primary_key=True)
    certification = Column(String, nullable=False)
    question = Column(String, nullable=False)
    answer = Column(String, nullable=False)
    link = Column(String)


class AWSQuestions(Questions):
    __tablename__ = "tb_aws_questions"


class AzureQuestions(Questions):
    __tablename__ = "tb_az_questions"


def drop():
    Base.metadata.drop_all(engine)


def create_table():
    # AzureQuestions.__table__,
    # e_name].__table__ for table_name in table_names
    Base.metadata.create_all(
        engine,
        tables=[AWSQuestions.__table__],
        checkfirst=True,
    )
