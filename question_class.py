class Question:
    def __init__(self, certification, question, answer, link):
        self._certification = certification
        self._question = question
        self._answer = answer
        self._link = link

    def __str__(self):
        return self._question + "\n" + self._answer + "\n" + self._link

    def __repr__(self):
        return self._question

    def __eq__(self, other):
        return self._question == other.question

    def __hash__(self):
        return hash(self._question)

    @property
    def _certification(self):
        return self._certification

    @property
    def _answer(self):
        return self._answer

    @property
    def _question(self):
        return self._question

    @property
    def _link(self):
        return self._link

    @_certification.setter
    def _certification(self, value):
        self._certification = value

    @_answer.setter
    def _answer(self, value):
        self._answer = value

    @_question.setter
    def _question(self, value):
        self._question = value

    @_link.setter
    def _link(self, value):
        self._link = value
