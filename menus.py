def main_menu():
    print("Welcome, choose an option below:")
    print("[1] - AWS")
    print("[2] - Azure")
    print("[3] - Quit")
    print("")
    option = input("Enter a option: ")
    return option


def aws_menu():
    print("Welcome to AWS Cloud Certifications Training")
    print("Choose a option below:")
    print("[1] - AWS Certified Cloud Practitioner")
    print("[2] - AWS Certified Data Engineer")
    print("[3] - Back to main menu")
    print("[4] - Quit")
    print("")
    option = input("Enter a option: ")
    return option


def aws_practitioner_menu():
    print("Welcome to AWS Certified Cloud Practitioner Training")
    print("Choose a option below:")
    print("[1] - Start")
    print("[2] - Filter by question number.")
    print("[3] - Filter by question text.")
    print("[4] - Add question")
    print("[5] - Remove question")
    print("[6] - Quit")
    print("")
    option = input("Enter a option: ")
    return option


def azure_menu():
    pass
