def logo():
    print(
        """
        ╔═╗┌─┐┬─┐┌┬┐┬┌─┐┬┌─┐┌─┐┌┬┐┬┌─┐┌┐┌  ╔╦╗┬─┐┌─┐┬┌┐┌┬┌┐┌┌─┐
        ║  ├┤ ├┬┘ │ │├┤ ││  ├─┤ │ ││ ││││   ║ ├┬┘├─┤│││││││││ ┬
        ╚═╝└─┘┴└─ ┴ ┴└  ┴└─┘┴ ┴ ┴ ┴└─┘┘└┘   ╩ ┴└─┴ ┴┴┘└┘┴┘└┘└─┘
        by Anderson Damasceno (BRQ Data Engineer)
        \n
        """
    )
